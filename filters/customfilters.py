#!/usr/bin/env python3
import pandocfilters as pf


def latex(s):
    return pf.RawBlock('latex', s)


ctx = {'col': False}


def apply_filter(k, v, f, m):
    if k == "Para":
        value = pf.stringify(v)
        if value.startswith('[') and value.endswith(']'):
            content = value[1:-1]
            # Answer environment
            if content == "answer":
                return latex(r'\begin{answerenv}')
            elif content == "/answer":
                return latex(r'\end{answerenv}')
            # only
            elif content == "/beameronly":
                return latex(r'}')
            elif content.find("beameronly") >= 0:
                return latex(r'\only<'+content[11:]+'>{')
            # Columns environment
            elif content == "columns":
                return latex(r'\begin{columns}')
            elif content == "/columns":
                ctx['col'] = False
                return latex(r'\end{column}\end{columns}')
            elif content.find('column') >= 0:
                w = r'0.48\textwidth'
                if len(content) >= 7 and content[6] == '=':
                    try:
                        w = str(float(content[7:]))+r'\textwidth'
                    except ValueError:
                        w = content[7:]
                col = r'\begin{column}{'+w+'}'
                if ctx['col']:
                    col = r'\end{column}' + col
                ctx['col'] = True
                return latex(col)


if __name__ == "__main__":
    pf.toJSONFilter(apply_filter)
