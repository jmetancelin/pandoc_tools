#!/usr/bin/env python3
import sys
import re

def match_ext_resources(l):
    if re.match('^<!--',l): # Comment in markdown
        return
    m = re.match(r'.*(includegraphics|pythonfile|bashfile)(\[.*\])?\{(?P<filename>[\w\d\-_\./]*)\}',l)
    if m:
        return m.group('filename')
    # Matching "![](img/mpi-types.png)"
    m = re.match(r"!\[.*]\((?P<filename>[a-zA-Z0-9/_.-]*)\)", l)
    if m:
        return m.group('filename')

def makedeps(fname):
    with open(fname) as f:
        return filter(lambda _: _, map(match_ext_resources, f.readlines()))

if __name__ == '__main__':
    print(sys.argv[1].replace('.md', '.pdf')+' : '+' '.join(makedeps(sys.argv[1])))
    print(sys.argv[1].replace('.md', '_handout.pdf')+' : '+' '.join(makedeps(sys.argv[1])))
