#!/bin/env python3
import sys

class Parse(object):
    """Documentation for Parse

    """
    def __init__(self):
        super(Parse, self).__init__()
        self.frame = -1
        self.answer = -1
        self.buff = []

    def get_buff_line(self, s):
        try:
            return list(filter(lambda _:_[1].find(s) >=0,enumerate(self.buff)))[0][0]
        except :
            return 0

    def __call__(self, line):
        if line.find('begin{frame}') == 1:
            self.frame = 0
        elif line.find('end{frame}') == 1:
            self.frame = -1
        else:
            if self.frame >= 0:
                self.frame += 1

        if line.find('begin{answerenv}') == 1:
            self.answer = 0
        elif line.find('end{answerenv}') == 1:
            self.answer = -1
        else:
            if self.answer >= 0:
                self.answer += 1
        l = line[:-1]
        if self.frame == -1:
            if len(self.buff) > 0:
                self.buff.append(l)
                answer_head = self.buff[self.get_buff_line('begin{frame}'):self.get_buff_line('begin{answerenv}')]
                answer_foot = self.buff[self.get_buff_line('end{answerenv}'):self.get_buff_line('end{frame}')]
                #print(f"%DEBUG {answer_head}, {answer_foot}")
                if len(list(filter(lambda _:_.find('begin{') <0 and _.find('hypertarget{') <0,
                              answer_head))) > 0 \
                   or len(list(filter(lambda _:_.find('end{') <0,
                                 answer_foot))) > 0:
                    print("\n".join(self.buff))
                else:
                    print("%" + "\n%".join(self.buff) + "\n%",)
                self.buff = []
            else:
                print(l)
        else:
            self.buff.append(l)


p = Parse()
for line in sys.stdin:
    p(line)
